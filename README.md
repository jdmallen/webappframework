# Web App Framework
## An alternative way to get started with ASP.NET MVC 5 & SQL Server 2014
*J.D. Mallen*

## Technologies used
* Repository pattern:
    * Contains separate project layers for database access and data.
    * Uses AutoMapper and Ninject to deserialize database models and then inject them into the controller constructors.
    * Depends on MVCHelpers project, elsewhere in my BitBucket.
* Dapper, the fast ORM from StackOverflow.
* Bootstrap.
* Configured for SQL Server 2014.
* Glimpse, a helpful diagnostics tool for analyzing site performance.

## Getting started
1. Structure your database.
    * Optionally, acquire the [AdventureWorks sample database](https://msftdbprodsamples.codeplex.com/downloads/get/880661) from Microsoft to see this app in action, as-is.
2. Create models in the context layer based on your raw database tables.
3. Create models in the data layer based on how those tables interact (i.e. foreign key relationships) using IEnumerable objects like Lists.
4. Configure your AppMappings in the context layer as needed so the database models map properly to the data models. Automapper will do most of the heavy lifting here for you.
5. Create repositories and interfaces for each model type, following the example provided.
6. Reference the repo interfaces in your controllers as needed, then call your repo methods to get data.

## Special thanks
Special thanks to [**Jonathon Leight**](https://bitbucket.org/jleight/) for researching and assembling the initial structure upon which this template was based, and to [**Joshua Peterson**](https://bitbucket.org/jpetey88/) for aiding in my initial understanding.

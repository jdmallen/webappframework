﻿using System.IO;
using System.Reflection;

namespace JDMallen.AppName.Data.Context.SQLQuery {
    internal static class SQLQuery {
        private const string QueryPath =
            @"JDMallen.AppName.Data.Context.SQLQuery.{0}.sql";

        // ReSharper disable UnusedAutoPropertyAccessor.Local
        public static string ReadAllPersonsTop100 { get; private set; }
        public static string ReadEmailsForPerson { get; private set; }
        // ReSharper restore UnusedAutoPropertyAccessor.Local

        static SQLQuery() {
            LoadQueries();
        }

        private static void LoadQueries() {
            PropertyInfo[] props =
                typeof (SQLQuery).GetProperties(BindingFlags.Public |
                                                 BindingFlags.Static);
            foreach (PropertyInfo prop in props) {
                string query = LoadQuery(prop);
                if (string.IsNullOrEmpty(query)) {
                    continue;
                }
                prop.SetValue(null, query);
            }
        }

        private static string LoadQuery(PropertyInfo property) {
            var assembly = Assembly.GetCallingAssembly();
            var path = string.Format(QueryPath, property.Name);
            var stream = assembly.GetManifestResourceStream(path);

            return stream == null
                ? null
                : new StreamReader(stream).ReadToEnd();
        }
    }
}

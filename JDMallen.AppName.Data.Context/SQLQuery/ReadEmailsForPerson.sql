SELECT [BusinessEntityID]
      ,[EmailAddressID]
      ,[EmailAddress]
      ,[rowguid]
      ,[ModifiedDate]
FROM [AdventureWorks].[Person].[EmailAddress]
WHERE [BusinessEntityID] = @personId
﻿using System.Data.SqlTypes;

namespace JDMallen.AppName.Data.Context.Models {
    public class DBEmail {
        public SqlInt32 BusinessEntityID { get; set; }
        public SqlInt32 EmailAddressID { get; set; }
        public SqlString EmailAddress { get; set; }
        public SqlDateTime ModifiedDate { get; set; }
    }
}
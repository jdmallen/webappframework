﻿using System.Data.SqlTypes;

namespace JDMallen.AppName.Data.Context.Models {
    public class DBPerson {
        public SqlInt32 BusinessEntityID { get; set; }
        public SqlString PersonType { get; set; }
        public SqlBoolean NameStyle { get; set; }
        public SqlString Title { get; set; }
        public SqlString FirstName { get; set; }
        public SqlString MiddleName { get; set; }
        public SqlString LastName { get; set; }
        public SqlString Suffix { get; set; }
        public SqlInt32 EmailPromotion { get; set; }
        public SqlString AdditionalContactInfo { get; set; }
        public SqlString Demographics { get; set; }
        public SqlDateTime ModifiedDate { get; set; }
    }
}
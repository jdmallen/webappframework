﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.Context.Config {
    public class AppContext : IContext {
        private IDbConnection _mainContext;

        internal IDbConnection MainDBContext
            =>
                _mainContext ??
                (_mainContext = GetConnection("ExampleConnection"));

        private static IDbConnection GetConnection(string name) {
            ConnectionStringSettings connectionSettings =
                ConfigurationManager.ConnectionStrings[name];
            if (connectionSettings == null)
                throw new InvalidOperationException(
                    "Invalid connection string name.");

            DbProviderFactory factory =
                DbProviderFactories.GetFactory(connectionSettings.ProviderName);
            DbConnection connection = factory.CreateConnection();
            if (connection == null)
                throw new InvalidOperationException("Invalid connection.");

            connection.ConnectionString = connectionSettings.ConnectionString;
            return connection;
        }

        public void SaveChanges() {
            throw new NotImplementedException();
        }
    }
}

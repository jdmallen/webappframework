﻿using JDMallen.AppName.Data.Context.Repos;
using JDMallen.AppName.Data.IRepos;
using Ninject.Modules;

namespace JDMallen.AppName.Data.Context.Config {
    public class AppNinjectModule : NinjectModule {
        public override void Load() {
            DoBind<AppContext,AppContext>();
            DoBind<IEmailRepo, EmailRepo>();
            DoBind<IPersonRepo,PersonRepo>();
        }

        private void DoBind<TInterface, TImplementation>()
            where TImplementation : TInterface {
            Bind<TInterface>().To<TImplementation>().InThreadScope();
        }
    }
}

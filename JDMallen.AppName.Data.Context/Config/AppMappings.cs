﻿using System.Drawing;
using System.IO;
using AutoMapper;
using JDMallen.AppName.Data.Context.Models;
using JDMallen.AppName.Data.Context.Repos;
using JDMallen.AppName.Data.Models;

namespace JDMallen.AppName.Data.Context.Config {
    internal class AppMappings : Profile {
        protected override void Configure() {
            // ReSharper disable UnusedVariable
            
            IMappingExpression<DBPerson, Person> personMapping =
                CreateMap<DBPerson, Person>();
            personMapping.ForMember(dest => dest.Emails,
                options =>
                    options.MapFrom(
                        src =>
                            EmailRepo.ResolveEmailsForPerson(new AppContext(),
                                src.BusinessEntityID.Value)));


            IMappingExpression<DBEmail, Email> emailMapping =
                CreateMap<DBEmail, Email>();
            emailMapping.ForMember(dest => dest.ID,
                options => options.MapFrom(src => src.EmailAddressID.Value));
            emailMapping.ForMember(dest => dest.PersonID,
                options => options.MapFrom(src => src.BusinessEntityID.Value));

            // ReSharper restore UnusedVariable
        }

        public static Image ByteArrayToImage(byte[] byteArrayIn) {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
    }
}

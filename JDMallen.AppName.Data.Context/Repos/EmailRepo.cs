﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using Dapper;
using JDMallen.AppName.Data.Context.Config;
using JDMallen.AppName.Data.Context.Models;
using JDMallen.AppName.Data.IRepos;
using JDMallen.AppName.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.Context.Repos {
    public class EmailRepo : Repo<AppContext, Email, int>, IEmailRepo {
        public EmailRepo(AppContext context) : base(context) {}

        public List<Email> ListEmailsForPerson(int personId) {
            return ResolveEmailsForPerson(Context, personId);
        }

        public static List<Email> ResolveEmailsForPerson(AppContext context, int personId) {
            return
                Mapper.Map<List<Email>>(
                    context.MainDBContext.Query<DBEmail>(
                        SQLQuery.SQLQuery.ReadEmailsForPerson, new {personId}));
        } 

        public override Email Get(int id) {
            throw new NotImplementedException();
        }
    }
}

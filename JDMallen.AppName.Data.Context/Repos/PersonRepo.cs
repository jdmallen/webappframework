﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using JDMallen.AppName.Data.Context.Config;
using JDMallen.AppName.Data.Context.Models;
using JDMallen.AppName.Data.IRepos;
using JDMallen.AppName.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.Context.Repos {
    public class PersonRepo :Repo<AppContext,Person,int>, IPersonRepo {
        public PersonRepo(AppContext context) : base(context) {}
        public override Person Get(int id) {
            throw new System.NotImplementedException();
        }

        public List<Person> GetAllPersons() {
            return
                Map<Person>(
                    Context.MainDBContext.Query<DBPerson>(
                        SQLQuery.SQLQuery.ReadAllPersonsTop100)).ToList();
        }
    }
}

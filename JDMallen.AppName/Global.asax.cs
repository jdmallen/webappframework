﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using JDMallen.AppName.Data.Context.Config;
using JDMallen.MVC.Helpers;
using Ninject;

namespace JDMallen.AppName {
    public class MvcApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            // http://blog.developers.ba/simple-way-share-container-mvc-web-api/
            AppNinjectModule registrations = new AppNinjectModule();
            StandardKernel kernel = new StandardKernel(registrations);
            NinjectDependencyResolver ninjectResolver =
                new NinjectDependencyResolver(kernel);
            DependencyResolver.SetResolver(ninjectResolver); // MVC
            GlobalConfiguration.Configuration.DependencyResolver =
                ninjectResolver; // Web API

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperProfileLoader.Load("JDMallen.AppName", "JDMallen.AppName.Data",
                "JDMallen.AppName.Data.Context");

            // http://stackoverflow.com/questions/12641386/failed-to-serialize-the-response-in-web-api
            GlobalConfiguration.Configuration.Formatters.JsonFormatter
                .SerializerSettings.ReferenceLoopHandling =
                Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters.Remove(
                GlobalConfiguration.Configuration.Formatters.XmlFormatter);
        }
    }
}

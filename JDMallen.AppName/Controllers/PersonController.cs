﻿using System.Web.Mvc;
using JDMallen.AppName.Data.IRepos;

namespace JDMallen.AppName.Controllers {
    public class PersonController : MasterController {

        public ActionResult Index() {
            return View(PersonRepo.GetAllPersons());
        }

        public PersonController(IEmailRepo emailRepo, IPersonRepo personRepo)
            : base(emailRepo, personRepo) {}
    }
}

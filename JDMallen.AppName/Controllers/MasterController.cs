﻿using System.Web.Http;
using System.Web.Mvc;
using JDMallen.AppName.Data.IRepos;
using Newtonsoft.Json;

namespace JDMallen.AppName.Controllers
{
    public abstract class MasterController : Controller {
        protected IEmailRepo EmailRepo { get; set; }
        protected IPersonRepo PersonRepo { get; set; }

        protected MasterController(IEmailRepo emailRepo, IPersonRepo personRepo) {
            EmailRepo = emailRepo;
            PersonRepo = personRepo;
        }

        public static string SuperSerialize(object obj) {
            return JsonConvert.SerializeObject(obj, Formatting.Indented);
        }
    }
}

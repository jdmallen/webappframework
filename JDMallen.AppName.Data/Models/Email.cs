﻿using System;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.Models {
    public class Email : Ent<int> {
        public new int ID { get; set; }
        public int PersonID { get; set; }
        public string EmailAddress { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.Models {
    public class Person : Ent<int> {
        public new int ID { get; set; }
        public string PersonType { get; set; }
        public bool NameStyle { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public int EmailPromotion { get; set; }
        public string AdditionalContactInfo { get; set; }
        public string Demographics { get; set; }
        public DateTime ModifiedDate { get; set; }
        public List<Email> Emails { get; set; } 
    }
}
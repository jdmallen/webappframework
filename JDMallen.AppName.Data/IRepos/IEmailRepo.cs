﻿using System.Collections.Generic;
using JDMallen.AppName.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.IRepos {
    public interface IEmailRepo : IRepo<Email> {
        List<Email> ListEmailsForPerson(int personId);
    }
}

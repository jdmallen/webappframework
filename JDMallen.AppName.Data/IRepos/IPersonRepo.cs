﻿using System.Collections.Generic;
using JDMallen.AppName.Data.Models;
using JDMallen.MVC.Helpers;

namespace JDMallen.AppName.Data.IRepos {
    public interface IPersonRepo : IRepo<Person,int> {
        List<Person> GetAllPersons();
    }
}
